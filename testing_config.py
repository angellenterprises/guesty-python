#!/usr/bin/env python
# coding: utf-8

from unittest import TestCase
from guesty.util import read_json


class BaseTestConfig(TestCase):

    json_fixtures = {}

    @classmethod
    def setUpClass(cls):
        cls.json_fixtures = read_json('./tests/fixtures/guesty_api.json')

