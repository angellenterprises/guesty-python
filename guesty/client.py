#!/usr/bin/env python
# coding: utf-8

from __future__ import unicode_literals
from typing import Any

import requests
import textwrap
import re
import json
from base64 import b64encode

from guesty import (
    api_base,
    api_version,
    error
)


def build_url(endpoint: str) -> str:
    url = api_base + '/' + api_version + '/'

    if endpoint:
        url += endpoint

    return url


def get_env() -> str:
    from guesty import env

    if not env:
        raise error.AuthenticationError(
            'No ENV provided. (HINT: set your ENV using '
            '"guesty.env = <DBRef>"). '
        )

    return env


def get_client_id() -> str:
    from guesty import api_client_id

    if api_client_id is None:
        raise error.AuthenticationError(
            'No API key provided. (HINT: set your API key using '
            'guesty.api_client_id = <API-KEY>).'
        )

    pattern = r'[0-9a-zA-Z]{32}'  # noqa: E501
    if not re.match(pattern, api_client_id):  # noqa: E501
        raise error.AuthenticationError(
            'Invalid API client id provided. (HINT: XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX).'
        )

    return api_client_id


def get_db_ref() -> Any:
    from guesty import api_db_ref

    return api_db_ref


def get_auth_headers():
    return {
        'content-type': "application/x-www-form-urlencoded",
        'cache-control': "no-cache",
        'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36'
    }


def revoke_token(auth_token=None):

    if not auth_token:
        raise error.AuthenticationError(
            'No Auth token provided. (HINT: set your Auth token using '
            '"guesty.auth_token = <AUTH-TOKEN>"). You can generate Auth Tokens keys '
            'from the guesty web interface.'
        )

    params = {
        'token': auth_token,
    }
    res = requests.delete(api_base, params=params, headers=get_auth_headers())
    data = json.loads(res.text)
    return 200


def get_token():
    from guesty import api_client_id, api_client_secret

    if api_client_secret is None:
        raise error.AuthenticationError(
            'No API key provided. (HINT: set your API key using '
            'guesty.api_client_secret = <API-KEY>).'
        )

    pattern = r'[0-9a-zA-Z]{32}'  # noqa: E501
    if not re.match(pattern, api_client_secret):  # noqa: E501
        raise error.AuthenticationError(
            'Invalid API secret provided. (HINT: XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX).'
        )

    return b64encode(bytes(f"{api_client_id}:{api_client_secret}", "utf-8")).decode("ascii")


def get_headers():
    from guesty import api_access_token

    if not api_access_token:
        raise error.AuthenticationError(
            'No Auth token provided. (HINT: set your Auth token using '
            '"guesty.api_access_token = <AUTH-TOKEN>"). You can generate Auth Tokens keys '
            'from the guesty web interface.'
        )
    return {
        'authorization': 'Bearer {}'.format(api_access_token),
        'accept': 'application/json',
        'content-type': 'application/json',
        'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36'
    }


def get_guesty_ref():
    from guesty import guesty_ref

    if not guesty_ref:
        raise error.AuthenticationError(
            'No Firestore DB provided. (HINT: set your Firestore DB using '
            '"guesty.guesty_ref = <DBRef>"). '
        )

    return guesty_ref


def get(url, params):
    try:
        print(url)
        res = requests.get(url, params=params, headers=get_headers())
    except Exception as e:
        handle_request_error(e)

    return handle_response(res)


def put(url, json):
    try:
        print(url)
        res = requests.put(url, headers=get_headers(), json=json)
    except Exception as e:
        handle_request_error(e)

    return {'status': res.text}


def delete(url):
    try:
        print(url)
        res = requests.delete(url, headers=get_headers())
    except Exception as e:
        handle_request_error(e)

    return handle_response(res)


def post(url, json):
    try:
        print(url)
        print(json)
        res = requests.post(url, data=json, headers=get_auth_headers())
    except Exception as e:
        handle_request_error(e)

    return handle_response(res)


def handle_response(res):
    try:
        json_res = res.json()
    except ValueError as e:
        try:
            json_res = {'error': res.text}
        except ValueError as e:
            handle_parse_error(e)

    if not (200 <= res.status_code < 300):
        handle_error_code(json_res, res.status_code, res.headers)

    return json_res


def handle_request_error(e):
    if isinstance(e, requests.exceptions.RequestException):
        msg = 'Unexpected error communicating with Guesty.'
        err = '{}: {}'.format(type(e).__name__, str(e))
        msg = textwrap.fill(msg) + '\n\n(Network error: {})'.format(err)
        raise error.APIConnectionError(msg)
    else:
        err = 'A {} was raised'.format(type(e).__name__)
        if u'%s' % e:
            err += ' with error message {}'.format(e)
        else:
            err += ' with no error message'
        raise error.APIConnectionError(err)


def handle_error_code(json, status_code, headers):
    if status_code == 400:
        err = json.get('error', 'Bad request')
        raise error.InvalidRequestError(err, status_code, headers)
    elif status_code == 401:
        err = json.get('error', 'Not authorized')
        raise error.AuthenticationError(err, status_code, headers)
    elif status_code == 404:
        err = json.get('message', 'no Route matched with those values')
        raise error.InvalidRequestError(err, status_code, headers)
    elif status_code == 500:
        err = json.get('error', 'Internal server error')
        raise error.APIError(err, status_code, headers)
    else:
        err = json.get('error', 'Unknown status code ({})'.format(status_code))
        raise error.APIError(err, status_code, headers)


def handle_parse_error(e, status_code=None, headers=None):
    err = '{}: {}'.format(type(e).__name__, e)
    msg = 'Error parsing guesty JSON response. \n\n{}'.format(err)
    raise error.APIError(msg, status_code, headers)
