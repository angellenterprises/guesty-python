api_base = 'https://open-api.guesty.com'
api_access_token = None
api_client_id = None
api_client_secret = None
api_version = 'v1'
env = 'production'
# DEMO
api_db_ref = None

from guesty.resource.base import (  # noqa
    Auth,
    GuestySdk,
)
