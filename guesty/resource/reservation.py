#!/usr/bin/env python
# coding: utf-8

from guesty.resource import GuestyAccountResource


class ReservationResource(GuestyAccountResource):

    @classmethod
    def list_url(cls) -> str:
        """
        Gets the GET url of this ReservationResource

        :return: The GET url of this ReservationResource.
        :rtype: str
        """
        return super(ReservationResource, cls).list_url() + 'reservations'

    @classmethod
    def list_v2_url(cls) -> str:
        """
        Gets the GET url of this ReservationResource

        :return: The GET url of this ReservationResource.
        :rtype: str
        """
        return super(ReservationResource, cls).list_v2_url() + 'reservations'