#!/usr/bin/env python
# coding: utf-8

from guesty.resource import GuestyAccountResource


class CalendarResource(GuestyAccountResource):

    @classmethod
    def get_url(cls, id: str) -> str:
        """
        Gets the GET url of this CalendarResource

        :return: The GET url of this CalendarResource.
        :rtype: str
        """
        return super(CalendarResource, cls).list_url() + 'listings/' + id + '/calendar'

    @classmethod
    def post_url(cls) -> str:
        """
        Gets the POST url of this CalendarResource

        :return: The POST url of this CalendarResource.
        :rtype: str
        """
        return super(CalendarResource, cls).list_url() + 'listings/calendars'