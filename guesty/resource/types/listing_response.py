#!/usr/bin/env python
# coding: utf-8

from guesty.resource import GuestyResource
from typing import List, Dict

class Address(GuestyResource):
    """
    Attributes:
      model_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    nullable = {
        'apt': True,
    }

    required = {
        'city': True,
        'country': True,
        'lat': True,
        'lng': True,
        'state': True,
        'street': True,
        'zipcode': True
    }

    model_types = {
        'apt': str,
        'city': str,
        'country': str,
        'lat': float,
        'lng': float,
        'state': str,
        'street': str,
        'zipcode': str
    }

    attribute_map = {
        'apt': 'apt',
        'city': 'city',
        'country': 'country',
        'lat': 'lat',
        'lng': 'lng',
        'state': 'state',
        'street': 'street',
        'zipcode': 'zipcode'
    }

    def refresh_from(cls, **kwargs):
        """Returns the dict as a model

        :param kwargs: A dict.
        :type: dict
        :return: The Currency of this Currency.  # noqa: E501
        :rtype: Currency
        """
        cls.sanity_check(kwargs)
        cls._apt = None
        cls._city = None
        cls._country = None
        cls._lat = None
        cls._lng = None
        cls._state = None
        cls._street = None
        cls._zipcode = None

        if 'apt' in kwargs:
            cls.apt = kwargs['apt']
        
        cls.city = kwargs['city']
        cls.country = kwargs['country']
        cls.lat = kwargs['lat']
        cls.lng = kwargs['lng']
        cls.state = kwargs['state']
        cls.street = kwargs['street']
        cls.zipcode = kwargs['zipcode']

    @property
    def apt(cls) -> str:
        """Gets the apt of this Address.


        :return: The apt of this Address.
        :rtype: str
        """
        return cls._apt

    @apt.setter
    def apt(cls, apt: str):
        """Sets the apt of this Address.


        :param apt: The apt of this Address.
        :type apt: str
        """

        cls._apt = apt

    @property
    def city(cls) -> str:
        """Gets the city of this Address.


        :return: The city of this Address.
        :rtype: str
        """
        return cls._city

    @city.setter
    def city(cls, city: str):
        """Sets the city of this Address.


        :param city: The city of this Address.
        :type city: str
        """
        if city is None:
            raise ValueError("Invalid value for `city`, must not be `None`")  # noqa: E501

        cls._city = city

    @property
    def country(cls) -> str:
        """Gets the country of this Address.


        :return: The country of this Address.
        :rtype: str
        """
        return cls._country

    @country.setter
    def country(cls, country: str):
        """Sets the country of this Address.


        :param country: The country of this Address.
        :type country: str
        """
        if country is None:
            raise ValueError("Invalid value for `country`, must not be `None`")  # noqa: E501

        cls._country = country

    @property
    def lat(cls) -> float:
        """Gets the lat of this Address.


        :return: The lat of this Address.
        :rtype: float
        """
        return cls._lat

    @lat.setter
    def lat(cls, lat: float):
        """Sets the lat of this Address.


        :param lat: The lat of this Address.
        :type lat: float
        """
        if lat is None:
            raise ValueError("Invalid value for `lat`, must not be `None`")  # noqa: E501

        cls._lat = lat

    @property
    def lng(cls) -> float:
        """Gets the lng of this Address.


        :return: The lng of this Address.
        :rtype: float
        """
        return cls._lng

    @lng.setter
    def lng(cls, lng: float):
        """Sets the lng of this Address.


        :param lng: The lng of this Address.
        :type lng: float
        """
        if lng is None:
            raise ValueError("Invalid value for `lng`, must not be `None`")  # noqa: E501

        cls._lng = lng

    @property
    def state(cls) -> str:
        """Gets the state of this Address.


        :return: The state of this Address.
        :rtype: str
        """
        return cls._state

    @state.setter
    def state(cls, state: str):
        """Sets the state of this Address.


        :param state: The state of this Address.
        :type state: str
        """
        if state is None:
            raise ValueError("Invalid value for `state`, must not be `None`")  # noqa: E501

        cls._state = state

    @property
    def street(cls) -> str:
        """Gets the street of this Address.


        :return: The street of this Address.
        :rtype: str
        """
        return cls._street

    @street.setter
    def street(cls, street: str):
        """Sets the street of this Address.


        :param street: The street of this Address.
        :type street: str
        """
        if street is None:
            raise ValueError("Invalid value for `street`, must not be `None`")  # noqa: E501

        cls._street = street

    @property
    def zipcode(cls) -> str:
        """Gets the zipcode of this Address.


        :return: The zipcode of this Address.
        :rtype: str
        """
        return cls._zipcode

    @zipcode.setter
    def zipcode(cls, zipcode: str):
        """Sets the zipcode of this Address.


        :param zipcode: The zipcode of this Address.
        :type zipcode: str
        """
        if zipcode is None:
            raise ValueError("Invalid value for `zipcode`, must not be `None`")  # noqa: E501

        cls._zipcode = zipcode



class Picture(GuestyResource):
    """
    Attributes:
      model_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    required = {
        'thumbnail': True
    }

    model_types = {
        'thumbnail': str,
    }

    attribute_map = {
        'thumbnail': 'thumbnail',
    }

    def refresh_from(cls, **kwargs):
        """Returns the dict as a model

        :param kwargs: A dict.
        :type: dict
        :return: The Currency of this Currency.  # noqa: E501
        :rtype: Currency
        """
        cls.sanity_check(kwargs)
        cls._thumbnail = None

        cls.thumbnail = kwargs['thumbnail']
    
    @property
    def thumbnail(cls) -> str:
        """Gets the thumbnail of this Picture.


        :return: The thumbnail of this Picture.
        :rtype: str
        """
        return cls._thumbnail

    @thumbnail.setter
    def thumbnail(cls, thumbnail: str):
        """Sets the thumbnail of this Picture.


        :param thumbnail: The thumbnail of this Picture.
        :type thumbnail: str
        """
        if thumbnail is None:
            raise ValueError("Invalid value for `thumbnail`, must not be `None`")  # noqa: E501

        cls._thumbnail = thumbnail


class Prices(GuestyResource):
    """
    Attributes:
      model_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    required = {
        'currency': True,
        'base_price': True,
        'cleaning_fee': True,
    }

    model_types = {
        'currency': str,
        'base_price': float,
        'cleaning_fee': float
    }

    attribute_map = {
        'currency': 'currency',
        'base_price': 'basePrice',
        'cleaning_fee': 'cleaningFee'
    }

    def refresh_from(cls, **kwargs):
        """Returns the dict as a model

        :param kwargs: A dict.
        :type: dict
        :return: The Currency of this Currency.  # noqa: E501
        :rtype: Currency
        """
        cls.sanity_check(kwargs)
        cls._currency = None
        cls._base_price = None
        cls._cleaning_fee = None

        cls.currency = kwargs['currency']
        cls.base_price = kwargs['basePrice']
        cls.cleaning_fee = kwargs['cleaningFee']

    @classmethod
    def from_dict(cls, dikt) -> 'Prices':
        """Returns the dict as a model

        :param dikt: A dict.
        :type: dict
        :return: The Prices of this Prices.  # noqa: E501
        :rtype: Prices
        """
        return util.deserialize_model(dikt, cls)

    @property
    def currency(cls) -> str:
        """Gets the currency of this Prices.


        :return: The currency of this Prices.
        :rtype: str
        """
        return cls._currency

    @currency.setter
    def currency(cls, currency: str):
        """Sets the currency of this Prices.


        :param currency: The currency of this Prices.
        :type currency: str
        """
        if currency is None:
            raise ValueError("Invalid value for `currency`, must not be `None`")  # noqa: E501

        cls._currency = currency

    @property
    def base_price(cls) -> float:
        """Gets the base_price of this Prices.


        :return: The base_price of this Prices.
        :rtype: float
        """
        return cls._base_price

    @base_price.setter
    def base_price(cls, base_price: float):
        """Sets the base_price of this Prices.


        :param base_price: The base_price of this Prices.
        :type base_price: float
        """
        if base_price is None:
            raise ValueError("Invalid value for `base_price`, must not be `None`")  # noqa: E501

        cls._base_price = base_price

    @property
    def cleaning_fee(cls) -> float:
        """Gets the cleaning_fee of this Prices.


        :return: The cleaning_fee of this Prices.
        :rtype: float
        """
        return cls._cleaning_fee

    @cleaning_fee.setter
    def cleaning_fee(cls, cleaning_fee: float):
        """Sets the cleaning_fee of this Prices.


        :param cleaning_fee: The cleaning_fee of this Prices.
        :type cleaning_fee: float
        """
        # if cleaning_fee is None:
            # raise ValueError("Invalid value for `cleaning_fee`, must not be `None`")  # noqa: E501

        cls._cleaning_fee = cleaning_fee



class ListingResponse(GuestyResource):
    """
    Attributes:
      model_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    nullable = {
        'default_check_in_time': True,
        'default_check_out_time': True,
    }
    required = {
        'id': True,
        'accommodates': True,
        'active': True,
        'address': True,
        'bathrooms': True,
        'bedrooms': True,
        'beds': True,
        'default_check_in_time': True,
        'default_check_out_time': True,
        'nickname': True,
        'picture': True,
        'prices': True
    }

    model_types = {
        'id': str,
        'accommodates': float,
        'active': bool,
        'address': dict,
        'bathrooms': float,
        'bedrooms': float,
        'beds': float,
        'default_check_in_time': str,
        'default_check_out_time': str,
        'nickname': str,
        'picture': dict,
        'prices': dict
    }

    attribute_map = {
        'id': '_id',
        'accommodates': 'accommodates',
        'active': 'active',
        'address': 'address',
        'bathrooms': 'bathrooms',
        'bedrooms': 'bedrooms',
        'beds': 'beds',
        'default_check_in_time': 'defaultCheckInTime',
        'default_check_out_time': 'defaultCheckOutTime',
        'nickname': 'nickname',
        'picture': 'picture',
        'prices': 'prices'
    }

    def refresh_from(cls, **kwargs):
        """Returns the dict as a model

        :param kwargs: A dict.
        :type: dict
        :return: The Currency of this Currency.  # noqa: E501
        :rtype: Currency
        """
        cls.sanity_check(kwargs)
        cls._id = None
        cls._accommodates = None
        cls._active = None
        cls._address = None
        cls._bathrooms = None
        cls._bedrooms = None
        cls._beds = None
        cls._default_check_in_time = None
        cls._default_check_out_time = None
        cls._nickname = None
        cls._picture = None
        cls._prices = None

        cls.id = kwargs['_id']
        cls.accommodates = kwargs['accommodates']
        cls.active = kwargs['active']
        cls.address = Address(**kwargs['address'])
        cls.bathrooms = kwargs['bathrooms']
        cls.bedrooms = kwargs['bedrooms']
        cls.beds = kwargs['beds']
        cls.default_check_in_time = kwargs['defaultCheckInTime']
        cls.default_check_out_time = kwargs['defaultCheckOutTime']
        cls.nickname = kwargs['nickname']
        cls.picture = Picture(**kwargs['picture'])
        cls.prices = Prices(**kwargs['prices'])

    @property
    def id(cls) -> str:
        """Gets the id of this ListingResponse.


        :return: The id of this ListingResponse.
        :rtype: str
        """
        return cls._id

    @id.setter
    def id(cls, id: str):
        """Sets the id of this ListingResponse.


        :param id: The id of this ListingResponse.
        :type id: str
        """
        if id is None:
            raise ValueError("Invalid value for `id`, must not be `None`")  # noqa: E501

        cls._id = id

    @property
    def accommodates(cls) -> float:
        """Gets the accommodates of this ListingResponse.


        :return: The accommodates of this ListingResponse.
        :rtype: float
        """
        return cls._accommodates

    @accommodates.setter
    def accommodates(cls, accommodates: float):
        """Sets the accommodates of this ListingResponse.


        :param accommodates: The accommodates of this ListingResponse.
        :type accommodates: float
        """
        if accommodates is None:
            raise ValueError("Invalid value for `accommodates`, must not be `None`")  # noqa: E501

        cls._accommodates = accommodates

    @property
    def active(cls) -> bool:
        """Gets the active of this ListingResponse.


        :return: The active of this ListingResponse.
        :rtype: bool
        """
        return cls._active

    @active.setter
    def active(cls, active: bool):
        """Sets the active of this ListingResponse.


        :param active: The active of this ListingResponse.
        :type active: bool
        """
        if active is None:
            raise ValueError("Invalid value for `active`, must not be `None`")  # noqa: E501

        cls._active = active

    @property
    def address(cls) -> Address:
        """Gets the address of this ListingResponse.


        :return: The address of this ListingResponse.
        :rtype: Address
        """
        return cls._address

    @address.setter
    def address(cls, address: Address):
        """Sets the address of this ListingResponse.


        :param address: The address of this ListingResponse.
        :type address: Address
        """
        if address is None:
            raise ValueError("Invalid value for `address`, must not be `None`")  # noqa: E501

        cls._address = address

    @property
    def bathrooms(cls) -> float:
        """Gets the bathrooms of this ListingResponse.


        :return: The bathrooms of this ListingResponse.
        :rtype: float
        """
        return cls._bathrooms

    @bathrooms.setter
    def bathrooms(cls, bathrooms: float):
        """Sets the bathrooms of this ListingResponse.


        :param bathrooms: The bathrooms of this ListingResponse.
        :type bathrooms: float
        """
        if bathrooms is None:
            raise ValueError("Invalid value for `bathrooms`, must not be `None`")  # noqa: E501

        cls._bathrooms = bathrooms

    @property
    def bedrooms(cls) -> float:
        """Gets the bedrooms of this ListingResponse.


        :return: The bedrooms of this ListingResponse.
        :rtype: float
        """
        return cls._bedrooms

    @bedrooms.setter
    def bedrooms(cls, bedrooms: float):
        """Sets the bedrooms of this ListingResponse.


        :param bedrooms: The bedrooms of this ListingResponse.
        :type bedrooms: float
        """
        if bedrooms is None:
            raise ValueError("Invalid value for `bedrooms`, must not be `None`")  # noqa: E501

        cls._bedrooms = bedrooms

    @property
    def beds(cls) -> float:
        """Gets the beds of this ListingResponse.


        :return: The beds of this ListingResponse.
        :rtype: float
        """
        return cls._beds

    @beds.setter
    def beds(cls, beds: float):
        """Sets the beds of this ListingResponse.


        :param beds: The beds of this ListingResponse.
        :type beds: float
        """
        if beds is None:
            raise ValueError("Invalid value for `beds`, must not be `None`")  # noqa: E501

        cls._beds = beds

    @property
    def default_check_in_time(cls) -> str:
        """Gets the default_check_in_time of this ListingResponse.


        :return: The default_check_in_time of this ListingResponse.
        :rtype: str
        """
        return cls._default_check_in_time

    @default_check_in_time.setter
    def default_check_in_time(cls, default_check_in_time: str):
        """Sets the default_check_in_time of this ListingResponse.


        :param default_check_in_time: The default_check_in_time of this ListingResponse.
        :type default_check_in_time: str
        """
        # if default_check_in_time is None:
        #     raise ValueError("Invalid value for `default_check_in_time`, must not be `None`")  # noqa: E501

        cls._default_check_in_time = default_check_in_time

    @property
    def default_check_out_time(cls) -> str:
        """Gets the default_check_out_time of this ListingResponse.


        :return: The default_check_out_time of this ListingResponse.
        :rtype: str
        """
        return cls._default_check_out_time

    @default_check_out_time.setter
    def default_check_out_time(cls, default_check_out_time: str):
        """Sets the default_check_out_time of this ListingResponse.


        :param default_check_out_time: The default_check_out_time of this ListingResponse.
        :type default_check_out_time: str
        """
        # if default_check_out_time is None:
        #     raise ValueError("Invalid value for `default_check_out_time`, must not be `None`")  # noqa: E501

        cls._default_check_out_time = default_check_out_time

    @property
    def nickname(cls) -> str:
        """Gets the nickname of this ListingResponse.


        :return: The nickname of this ListingResponse.
        :rtype: str
        """
        return cls._nickname

    @nickname.setter
    def nickname(cls, nickname: str):
        """Sets the nickname of this ListingResponse.


        :param nickname: The nickname of this ListingResponse.
        :type nickname: str
        """
        if nickname is None:
            raise ValueError("Invalid value for `nickname`, must not be `None`")  # noqa: E501

        cls._nickname = nickname

    @property
    def picture(cls) -> Picture:
        """Gets the picture of this ListingResponse.


        :return: The picture of this ListingResponse.
        :rtype: Picture
        """
        return cls._picture

    @picture.setter
    def picture(cls, picture: Picture):
        """Sets the picture of this ListingResponse.


        :param picture: The picture of this ListingResponse.
        :type picture: Picture
        """
        if picture is None:
            raise ValueError("Invalid value for `picture`, must not be `None`")  # noqa: E501

        cls._picture = picture

    @property
    def prices(cls) -> Prices:
        """Gets the prices of this ListingResponse.


        :return: The prices of this ListingResponse.
        :rtype: Prices
        """
        return cls._prices

    @prices.setter
    def prices(cls, prices: Prices):
        """Sets the prices of this ListingResponse.


        :param prices: The prices of this ListingResponse.
        :type prices: Prices
        """
        if prices is None:
            raise ValueError("Invalid value for `prices`, must not be `None`")  # noqa: E501

        cls._prices = prices
