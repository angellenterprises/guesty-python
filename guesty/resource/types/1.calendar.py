#!/usr/bin/env python
# coding: utf-8

from __future__ import unicode_literals
from guesty import client
from guesty.resource import GuestyAccountResource
import json


def format_array(listingHash, batch_days_array):
    new_array = []
    for day in batch_days_array:
        day['listingId'] = listingHash
        new_array.append(day)
    return new_array


class Calendar(GuestyAccountResource):

    @classmethod
    def list_url(cls):
        return super(Calendar, cls).list_url(None) + 'listings/calendars'

    @classmethod
    def get_url(cls, id):
        return super(Calendar, cls).list_url(None) + 'listings/' + id + '/calendar'

    @classmethod
    def update(cls, listingHash, batch_days_array):
        batch_days_array = format_array(listingHash, batch_days_array)
        res = client.put(cls.list_url(), batch_days_array)
        print(res)
        return res

    def refresh_from(cls, **kwargs):
        # print('GUESTY CALENDAR: {}'.format(json.dumps(kwargs, indent=4, sort_keys=True)))
        days_collection = []
        for day in kwargs['days']:
            days_collection.append(Day(cls.account_id, **day))
        cls.days = days_collection

    def to_any_object(cls):
        days_collection = []
        for day in cls.days:
            days_collection.append(day.to_any_object())
        return {
            'days': days_collection,
        }


class Day(GuestyAccountResource):

    def refresh_from(cls, **kwargs):
        # cls.v = None
        cls.id = None
        # cls.accountId = None
        # if '__v' in kwargs:
        #     cls.v = kwargs['__v']
        if '_id' in kwargs:
            cls.id = kwargs['_id']
        # if 'accountId' in kwargs:
        #     cls.accountId = kwargs['accountId']

        cls.blocks = DayBlocks(cls.account_id, **kwargs['blocks'])
        cls.currency = kwargs['currency']
        cls.date = kwargs['date']
        # cls.listing = DayListing(cls.account_id, **kwargs['listing'])
        # cls.listingId = kwargs['listingId']
        cls.price = kwargs['price']
        cls.status = kwargs['status']

    def to_any_object(cls):
        return {
            # '__v': cls.v,
            '_id': cls.id,
            # 'accountId': cls.accountId,
            'blocks': cls.blocks.to_any_object(),
            'currency': cls.currency,
            'date': cls.date,
            # 'listing': cls.listing.to_any_object(),
            # 'listingId': cls.listingId,
            'price': cls.price,
            'status': cls.status,
        }


class DayBlocks(GuestyAccountResource):

    def refresh_from(cls, **kwargs):
        cls.a = kwargs['a']
        cls.abl = kwargs['abl']
        cls.b = kwargs['b']
        cls.bd = kwargs['bd']
        cls.bw = kwargs['bw']
        cls.m = kwargs['m']
        cls.o = kwargs['o']
        cls.r = kwargs['r']
        cls.sr = kwargs['sr']

    def to_any_object(cls):
        return {
            'a': cls.a,
            'abl': cls.abl,
            'b': cls.b,
            'bd': cls.bd,
            'bw': cls.bw,
            'm': cls.m,
            'o': cls.o,
            'r': cls.r,
            'sr': cls.sr,
        }


class DayListing(GuestyAccountResource):

    def refresh_from(cls, **kwargs):
        cls.id = kwargs['_id']
        cls.prices = kwargs['prices']

    def to_any_object(cls):
        return {
            '_id': cls.id,
            'prices': cls.prices
        }
