#!/usr/bin/env python
# coding: utf-8

from guesty.resource import GuestyResource
from typing import List, Dict

class Money(GuestyResource):
    """
    Attributes:
      model_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    required = {
        'currency': str,
        'fare_accommodation': float,
        'fare_cleaning': float
    }

    model_types = {
        'currency': str,
        'fare_accommodation': float,
        'fare_cleaning': float
    }

    attribute_map = {
        'currency': 'currency',
        'fare_accommodation': 'fareAccommodation',
        'fare_cleaning': 'fareCleaning'
    }

    def refresh_from(cls, **kwargs):
        """Returns the dict as a model

        :param kwargs: A dict.
        :type: dict
        :return: The Currency of this Currency.  # noqa: E501
        :rtype: Currency
        """
        cls.sanity_check(kwargs)

        cls._currency = None
        cls._fare_accommodation = None
        cls._fare_cleaning = None

        cls.currency = kwargs['currency']
        cls.fare_accommodation = kwargs['fareAccommodation']
        cls.fare_cleaning = kwargs['fareCleaning']

    @property
    def currency(self) -> str:
        """Gets the currency of this Money.


        :return: The currency of this Money.
        :rtype: str
        """
        return self._currency

    @currency.setter
    def currency(self, currency: str):
        """Sets the currency of this Money.


        :param currency: The currency of this Money.
        :type currency: str
        """
        if currency is None:
            raise ValueError("Invalid value for `currency`, must not be `None`")  # noqa: E501

        self._currency = currency

    @property
    def fare_accommodation(self) -> float:
        """Gets the fare_accommodation of this Money.


        :return: The fare_accommodation of this Money.
        :rtype: float
        """
        return self._fare_accommodation

    @fare_accommodation.setter
    def fare_accommodation(self, fare_accommodation: float):
        """Sets the fare_accommodation of this Money.


        :param fare_accommodation: The fare_accommodation of this Money.
        :type fare_accommodation: float
        """
        if fare_accommodation is None:
            raise ValueError("Invalid value for `fare_accommodation`, must not be `None`")  # noqa: E501

        self._fare_accommodation = fare_accommodation

    @property
    def fare_cleaning(self) -> float:
        """Gets the fare_cleaning of this Money.


        :return: The fare_cleaning of this Money.
        :rtype: float
        """
        return self._fare_cleaning

    @fare_cleaning.setter
    def fare_cleaning(self, fare_cleaning: float):
        """Sets the fare_cleaning of this Money.


        :param fare_cleaning: The fare_cleaning of this Money.
        :type fare_cleaning: float
        """
        if fare_cleaning is None:
            raise ValueError("Invalid value for `fare_cleaning`, must not be `None`")  # noqa: E501

        self._fare_cleaning = fare_cleaning


class ReservationResponse(GuestyResource):
    """
    Attributes:
      model_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """

    nullable = {
        'confirmation_code': True,
    }

    required = {
        'id': True,
        'nights_count': True,
        'created_at': True,
        'check_in': True,
        'check_out': True,
        # 'guest_id': True,
        'listing_id': True,
        'status': True,
        'money': True
    }

    model_types = {
        'id': str,
        'nights_count': int,
        'created_at': str,
        'check_in': str,
        'check_out': str,
        'confirmation_code': str,
        'guest_id': str,
        'listing_id': str,
        'status': str,
        'money': dict
    }

    attribute_map = {
        'id': '_id',
        'nights_count': 'nightsCount',
        'created_at': 'createdAt',
        'check_in': 'checkIn',
        'check_out': 'checkOut',
        'confirmation_code': 'confirmationCode',
        'guest_id': 'guestId',
        'listing_id': 'listingId',
        'status': 'status',
        'money': 'money'
    }

    def refresh_from(cls, **kwargs):
        """Returns the dict as a model

        :param kwargs: A dict.
        :type: dict
        :return: The Currency of this Currency.  # noqa: E501
        :rtype: Currency
        """
        cls.sanity_check(kwargs)

        cls._id = None
        cls._nights_count = None
        cls._created_at = None
        cls._check_in = None
        cls._check_out = None
        cls._confirmation_code = None
        cls._guest_id = None
        cls._listing_id = None
        cls._status = None
        cls._money = None

        cls.id = kwargs['_id']
        cls.nights_count = kwargs['nightsCount']
        cls.created_at = kwargs['createdAt']
        cls.check_in = kwargs['checkIn']
        cls.check_out = kwargs['checkOut']
        if 'confirmationCode' in kwargs:
            cls.confirmation_code = kwargs['confirmationCode']
        if 'guestId' in kwargs:
            cls.guest_id = kwargs['guestId']
        cls.listing_id = kwargs['listingId']
        cls.status = kwargs['status']
        cls.money = Money(**kwargs['money'])

    @property
    def id(cls) -> str:
        """Gets the id of this ReservationResponse.


        :return: The id of this ReservationResponse.
        :rtype: str
        """
        return cls._id

    @id.setter
    def id(cls, id: str):
        """Sets the id of this ReservationResponse.


        :param id: The id of this ReservationResponse.
        :type id: str
        """
        if id is None:
            raise ValueError("Invalid value for `id`, must not be `None`")  # noqa: E501

        cls._id = id

    @property
    def nights_count(cls) -> float:
        """Gets the nights_count of this ReservationResponse.


        :return: The nights_count of this ReservationResponse.
        :rtype: float
        """
        return cls._nights_count

    @nights_count.setter
    def nights_count(cls, nights_count: float):
        """Sets the nights_count of this ReservationResponse.


        :param nights_count: The nights_count of this ReservationResponse.
        :type nights_count: float
        """
        if nights_count is None:
            raise ValueError("Invalid value for `nights_count`, must not be `None`")  # noqa: E501

        cls._nights_count = nights_count

    @property
    def created_at(cls) -> str:
        """Gets the created_at of this ReservationResponse.


        :return: The created_at of this ReservationResponse.
        :rtype: str
        """
        return cls._created_at

    @created_at.setter
    def created_at(cls, created_at: str):
        """Sets the created_at of this ReservationResponse.


        :param created_at: The created_at of this ReservationResponse.
        :type created_at: str
        """
        if created_at is None:
            raise ValueError("Invalid value for `created_at`, must not be `None`")  # noqa: E501

        cls._created_at = created_at

    @property
    def check_in(cls) -> str:
        """Gets the check_in of this ReservationResponse.


        :return: The check_in of this ReservationResponse.
        :rtype: str
        """
        return cls._check_in

    @check_in.setter
    def check_in(cls, check_in: str):
        """Sets the check_in of this ReservationResponse.


        :param check_in: The check_in of this ReservationResponse.
        :type check_in: str
        """
        if check_in is None:
            raise ValueError("Invalid value for `check_in`, must not be `None`")  # noqa: E501

        cls._check_in = check_in

    @property
    def check_out(cls) -> str:
        """Gets the check_out of this ReservationResponse.


        :return: The check_out of this ReservationResponse.
        :rtype: str
        """
        return cls._check_out

    @check_out.setter
    def check_out(cls, check_out: str):
        """Sets the check_out of this ReservationResponse.


        :param check_out: The check_out of this ReservationResponse.
        :type check_out: str
        """
        if check_out is None:
            raise ValueError("Invalid value for `check_out`, must not be `None`")  # noqa: E501

        cls._check_out = check_out

    @property
    def confirmation_code(cls) -> str:
        """Gets the confirmation_code of this ReservationResponse.


        :return: The confirmation_code of this ReservationResponse.
        :rtype: str
        """
        return cls._confirmation_code

    @confirmation_code.setter
    def confirmation_code(cls, confirmation_code: str):
        """Sets the confirmation_code of this ReservationResponse.


        :param confirmation_code: The confirmation_code of this ReservationResponse.
        :type confirmation_code: str
        """
        # if confirmation_code is None:
        #     raise ValueError("Invalid value for `confirmation_code`, must not be `None`")  # noqa: E501

        cls._confirmation_code = confirmation_code

    @property
    def guest_id(cls) -> str:
        """Gets the guest_id of this ReservationResponse.


        :return: The guest_id of this ReservationResponse.
        :rtype: str
        """
        return cls._guest_id

    @guest_id.setter
    def guest_id(cls, guest_id: str):
        """Sets the guest_id of this ReservationResponse.


        :param guest_id: The guest_id of this ReservationResponse.
        :type guest_id: str
        """
        # if guest_id is None:
            # raise ValueError("Invalid value for `guest_id`, must not be `None`")  # noqa: E501

        cls._guest_id = guest_id

    @property
    def listing_id(cls) -> str:
        """Gets the listing_id of this ReservationResponse.


        :return: The listing_id of this ReservationResponse.
        :rtype: str
        """
        return cls._listing_id

    @listing_id.setter
    def listing_id(cls, listing_id: str):
        """Sets the listing_id of this ReservationResponse.


        :param listing_id: The listing_id of this ReservationResponse.
        :type listing_id: str
        """
        if listing_id is None:
            raise ValueError("Invalid value for `listing_id`, must not be `None`")  # noqa: E501

        cls._listing_id = listing_id

    @property
    def status(cls) -> str:
        """Gets the status of this ReservationResponse.


        :return: The status of this ReservationResponse.
        :rtype: str
        """
        return cls._status

    @status.setter
    def status(cls, status: str):
        """Sets the status of this ReservationResponse.


        :param status: The status of this ReservationResponse.
        :type status: str
        """
        if status is None:
            raise ValueError("Invalid value for `status`, must not be `None`")  # noqa: E501

        cls._status = status

    @property
    def money(cls) -> Money:
        """Gets the money of this ReservationResponse.


        :return: The money of this ReservationResponse.
        :rtype: Money
        """
        return cls._money

    @money.setter
    def money(cls, money: Money):
        """Sets the money of this ReservationResponse.


        :param money: The money of this ReservationResponse.
        :type money: Money
        """
        if money is None:
            raise ValueError("Invalid value for `money`, must not be `None`")  # noqa: E501

        cls._money = money
