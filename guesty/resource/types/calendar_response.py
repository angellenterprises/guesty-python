#!/usr/bin/env python
# coding: utf-8

from guesty.resource import GuestyResource
from typing import List


class Day(GuestyResource):
    """
    Attributes:
      model_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    required = {
        'currency': True,
        '_date': True,
        'price': True,
        'status': True
    }

    model_types = {
        'currency': str,
        '_date': str,
        'price': float,
        'status': str
    }

    attribute_map = {
        'currency': 'currency',
        '_date': 'date',
        'price': 'price',
        'status': 'status'
    }

    def refresh_from(cls, **kwargs):
        """Returns the dict as a model

        :param kwargs: A dict.
        :type: dict
        :return: The Day of this Day.  # noqa: E501
        :rtype: Day
        """
        cls.sanity_check(kwargs)

        cls._currency = None
        cls.__date = None
        cls._price = None
        cls._status = None

        cls.currency = kwargs['currency']
        cls._date = kwargs['date']
        cls.price = kwargs['price']
        cls.status = kwargs['status']

    @property
    def currency(cls) -> str:
        """Gets the currency of this Day.


        :return: The currency of this Day.
        :rtype: str
        """
        return cls._currency

    @currency.setter
    def currency(cls, currency: str):
        """Sets the currency of this Day.


        :param currency: The currency of this Day.
        :type currency: str
        """
        if currency is None:
            raise ValueError("Invalid value for `currency`, must not be `None`")  # noqa: E501

        cls._currency = currency

    @property
    def _date(cls) -> str:
        """Gets the _date of this Day.


        :return: The _date of this Day.
        :rtype: str
        """
        return cls.__date

    @_date.setter
    def _date(cls, _date: str):
        """Sets the _date of this Day.


        :param _date: The _date of this Day.
        :type _date: str
        """
        if _date is None:
            raise ValueError("Invalid value for `_date`, must not be `None`")  # noqa: E501

        cls.__date = _date

    @property
    def price(cls) -> float:
        """Gets the price of this Day.


        :return: The price of this Day.
        :rtype: float
        """
        return cls._price

    @price.setter
    def price(cls, price: float):
        """Sets the price of this Day.


        :param price: The price of this Day.
        :type price: float
        """
        if price is None:
            raise ValueError("Invalid value for `price`, must not be `None`")  # noqa: E501

        cls._price = price

    @property
    def status(cls) -> str:
        """Gets the status of this Day.


        :return: The status of this Day.
        :rtype: str
        """
        return cls._status

    @status.setter
    def status(cls, status: str):
        """Sets the status of this Day.


        :param status: The status of this Day.
        :type status: str
        """
        if status is None:
            raise ValueError("Invalid value for `status`, must not be `None`")  # noqa: E501

        cls._status = status


class CalendarResponse(GuestyResource):
    """
    Attributes:
      model_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    required = {
        'days': True,
    }

    model_types = {
        'days': list,
    }

    attribute_map = {
        'days': 'days',
    }

    def refresh_from(cls, **kwargs):
        """Returns the dict as a model

        :param kwargs: A dict.
        :type: dict
        :return: The CalendarResponse of this CalendarResponse.  # noqa: E501
        :rtype: CalendarResponse
        """
        cls.sanity_check(kwargs)
        cls._days = None

        cls.days = [Day(**d) for d in kwargs['days']]

    @property
    def days(self) -> List[Day]:
        """Gets the days of this CalendarResponse.


        :return: The days of this CalendarResponse.
        :rtype: List[Day]
        """
        return self._days

    @days.setter
    def days(self, days: List[Day]):
        """Sets the days of this CalendarResponse.


        :param days: The days of this CalendarResponse.
        :type days: List[Day]
        """
        if days is None:
            raise ValueError("Invalid value for `days`, must not be `None`")  # noqa: E501

        self._days = days