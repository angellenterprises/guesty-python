#!/usr/bin/env python
# coding: utf-8

import json
import math
from typing import List, Dict

from guesty import client, error
from guesty.resource import AuthResource, GuestyResource

from .types import (
    ListingResponse,
    CalendarResponse,
    ReservationResponse,
)

from guesty.resource.listing import ListingResource
from guesty.resource.calendar import CalendarResource
from guesty.resource.reservation import ReservationResource
# mock
from guesty.util import (
    get_currency_code,
)
from guesty.mock import (
    get_mock_listings,
    get_mock_calendar,
    get_mock_reservations,
)


class Auth(AuthResource):

    def refresh_from(cls, **kwargs):
        cls.access_token = kwargs['access_token']
        cls.expires_in = kwargs['expires_in']
        cls.token_type = kwargs['token_type']
        cls.scope = 'general'


class GuestySdk(GuestyResource):

    def refresh_from(cls, **kwargs):
        """refresh_from."""
        cls.id = kwargs['id']
        cls.expires_in = kwargs['expires_in']
        cls.token_type = kwargs['token_type']
        cls.scope = kwargs['scope']

    def to_any_object(cls):
        """to_any_object."""
        return {
            'id': cls.id,
            'expires_in': cls.expires_in,
            'token_type': cls.token_type,
            'scope': cls.scope,
        }

    def get_listings(cls) -> List[ListingResponse]:
        """Returns the dict as a model

        :return: The ListingResponse of this ListingResponse.  # noqa: E501
        :rtype: ListingResponse
        """

        new_listings = []
        params = {
            'limit': 25,
        }
        if client.get_env() == 'sandbox':
            res = {}
            res['results'] = get_mock_listings(
                cls.id,
                currency_code=get_currency_code(client.get_client_id()) or 'USD'
            )
            res['count'] = 5
            res['limit'] = 25
        else:
            res = client.get(ListingResource.list_url(), params)

        # Pagination
        total_count = 0
        if 'count' in res:
            total_count = res['count']
        if 'limit' in res:
            limit = res['limit']
        num_pages = math.ceil(total_count / limit)

        # First Results
        res = res['results']
        new_listings.extend([ListingResponse(**l) for l in res])
        # More Results
        for page in range(1, num_pages):
            params = {}
            params['skip'] = page * limit
            params['limit'] = limit
            if client.get_env() == 'sandbox':
                return new_listings
            else:
                res = client.get(ListingResource.list_url(), params)
            res = res['results']
            new_listings.extend([ListingResponse(**i) for i in res])
        return new_listings

    def get_calendar(
        cls,
        id: str,
        start_date: str,
        end_date: str
    ) -> CalendarResponse:
        """Returns the dict as a model

        :return: The CalendarResponse of this CalendarResponse.  # noqa: E501
        :rtype: CalendarResponse
        """

        if not isinstance(start_date, str):
            raise error.InvalidRequestError('Start Date')

        if not isinstance(end_date, str):
            raise error.InvalidRequestError('End Date')

        params = {
            'from': start_date,
            'to': end_date,
        }
        res = {}
        if client.get_env() == 'sandbox':
            days = get_mock_calendar(
                id=id, 
                params=params,
                currency_code=get_currency_code(client.get_client_id())
            )
            res['days'] = days
        else:
            days = client.get(CalendarResource.get_url(id), params)
            res['days'] = days

        return CalendarResponse(**res)

    def get_reservations(
        cls, 
        id: str, 
        start_date: str, 
        end_date: str
    ) -> List[ReservationResponse]:
        """Returns the dict as a model

        :return: The ReservationResponse of this ReservationResponse.  # noqa: E501
        :rtype: ReservationResponse
        """

        if not isinstance(start_date, str):
            raise error.InvalidRequestError('Start Date')

        if not isinstance(end_date, str):
            raise error.InvalidRequestError('End Date')

        # num_pages = 0
        # per_page = 0
        new_reservations = []
        filters = []
        limit = 25
        filters.append({
            'field': 'listingId',
            'operator': '$eq',
            'value': id,
            'context': None
        })
        if start_date:
            filters.append({
                'field': 'checkIn',
                'operator': '$gt',
                'value': start_date,
                'context': None
            })
        params = {
            'filters': json.dumps(filters),
            'fields': 'nightsCount checkIn checkOut createdAt money status confirmationCode',
        }

        if client.get_env() == 'sandbox':
            res = {}
            reservations = get_mock_reservations(
                id=id,
                start_date=start_date,
                end_date=end_date,
                currency_code=get_currency_code(client.get_client_id())
            )
            res['results'] = reservations
            res['count'] = len(reservations)
            res['limit'] = limit
        else:
            res = client.get(ReservationResource.list_url(), params)

        # Pagination
        total_count = 0
        if 'count' in res:
            total_count = res['count']
        if 'limit' in res:
            limit = res['limit']
        num_pages = math.ceil(total_count / limit)

        # Add First Page Results
        res = res['results']
        new_reservations.extend(ReservationResponse(**r) for r in res)

        # More Results
        for page in range(1, num_pages):
            params['skip'] = page * limit
            params['limit'] = limit
            # For Sandbox not having pages
            if client.get_env() == 'sandbox':
                return new_reservations
            else:
                res = client.get(ReservationResource.list_url(), params)
            res = res['results']
            new_reservations.extend([ReservationResponse(**r) for r in res])

        return new_reservations

    def update_calendar(
        cls,
        id: str,
        batch_array: List[Dict]
    ) -> Dict:
        """Returns the dict as a model

        :return: The dict of this dict.  # noqa: E501
        :rtype: dict
        """
        if client.get_env() == 'sandbox':
            return {'status': 'ok', 'test': True}

        def format_array(
            id: str, 
            batch_days_array: List[Dict]
        )  -> List[Dict]:
            new_array = []
            for day in batch_days_array:
                day['listingId'] = id
                new_array.append(day)
            return new_array
        
        batch_days_array = format_array(id, batch_array)
        return client.put(CalendarResource.post_url(), batch_days_array)

    def __unicode__(cls):
        return '<{} {}>'.format(cls.__class__.__name__, cls.id)
