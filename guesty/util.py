#!/usr/bin/env python
# coding: utf-8

import re
import json
from typing import Any, Dict
import logging

from guesty.client import get_db_ref

# Create Logger
logger = logging.getLogger('app')

class CachedProperty(object):
    """
    A property that is only computed once per instance and then replaces
    itself with an ordinary attribute. Deleting the attribute resets the
    property.

    Taken from https://github.com/bottlepy/bottle/blob/master/bottle.py
    """

    # TODO: allow refresh

    def __init__(self, func):
        self.__doc__ = getattr(func, '__doc__')
        self.func = func

    def __get__(self, obj, cls):
        if obj is None:
            return self

        value = obj.__dict__[self.func.__name__] = self.func(obj)
        return value


cached_property = CachedProperty


def get_included_object(obj):
    try:
        rel = obj['_included'][0]['rel']
        data = obj['_included'][0]['data']
        return rel, data
    except:
        return None


def has_included_objects(obj):
    return '_included' in obj


def get_currency_code(client_id: str):
    currency_code = client_id[0:3]
    return currency_code


def read_json(path):
    with open(path) as json_file:
        return json.load(json_file)


def write_json(data, path):
    with open(path, 'w') as json_file:
        json.dump(data, json_file)


def read_fs_doc(
    collection_id: str, 
    document_id: str
) -> Dict:
    try:
        db_ref: Any = get_db_ref()
        reference: Any = db_ref.collection(collection_id).document(document_id)
        response: Any = reference.get()
        if not response.exists:
            return {}
        
        if not response.to_dict():
            raise ValueError('[MOCK] Invalid Collection/Document Data')
        
        return response.to_dict()
    except Exception as e:
        raise e

def read_fs_docs(
    collection_id: str,
) -> Dict:
    try:
        db_ref: Any = get_db_ref()
        reference: Any = db_ref.collection(collection_id)
        response: Any = reference.stream()
        return [data.to_dict() for data in response]
    except Exception as e:
        raise e

def write_fs_doc(
    collection_id: str, 
    document_id: str,
    data: Dict,
) -> bool:
    try:
        db_ref: Any = get_db_ref()
        reference: Any = db_ref.collection(collection_id).document(document_id)
        response: Any = reference.get()
        if not response.exists:
            logger.info('[MOCK] CREATING: {}'.format(collection_id))
            reference.set(data)
            return 200
        logger.info('[MOCK] UPDATING: {}'.format(collection_id))
        reference.update(data)
        return 200
    except Exception as e:
        raise e


def from_decimal_to_int(decimal):
    decimal = '{0:.2f}'.format(float(decimal))
    return int(re.sub('.', '', decimal))


def from_int_to_decimal(integer):
    string = str(integer)
    return round(float('{}.{}'.format(string[:-2], string[-2:])), 2)
