#!/usr/bin/env python
# coding: utf-8

import os
import random
import logging

import uuid
from typing import List, Any
from datetime import datetime, timedelta

from guesty.resource.types.calendar_response import Day

from guesty.util import (
    read_fs_docs, 
    read_fs_doc, 
    write_fs_doc
)

# Create Logger
logger = logging.getLogger('app')

random_address_ends = [
    'Ave',
    'St',
    'Way',
]


def get_rate(currency_code: str) -> float:
    if currency_code == 'USD':
        return 1
    if currency_code == 'EUR':
        return 0.97
    if currency_code == 'MXN':
        return 19.86
    return 1


def get_month_start(year=None, month=None):
    return datetime.strptime('{}-{}-{}'.format(year, month, 1), '%Y-%m-%d')


def get_month_end(dt=None):
    next_dt = dt.replace(day=28) + timedelta(days=4)  # this will never fail
    return next_dt - timedelta(days=next_dt.day)


def backwards_month(month=0):
    if month == 0:
        return 12
    if month == -1:
        return 11
    if month == -2:
        return 10
    if month == -3:
        return 9
    if month == -4:
        return 8
    if month == -5:
        return 7
    if month == -6:
        return 6
    if month == -7:
        return 5
    if month == -8:
        return 4
    if month == -9:
        return 3
    if month == -10:
        return 2
    if month == -11:
        return 1


def dates_bwn_twodates(start_date, end_date):
    for n in range(int ((end_date - start_date).days) + 1):
        yield start_date + timedelta(n)


def to_dt(string):
    return datetime.strptime(string, "%Y-%m-%d")


def to_dtl(string):
    return datetime.strptime(string, "%Y-%m-%dT%H:%M:%S")


def from_dt(date):
    return datetime.strftime(date, "%Y-%m-%d")


def from_dtl(date):
    return datetime.strftime(date, "%Y-%m-%dT%H:%M:%S")


def random_blocks(selected):
    dict = {}
    dict['a'] = None
    dict['abl'] = None
    dict['b'] = None # booked
    dict['bd'] = None
    dict['bw'] = None
    dict['m'] = None #  manual
    dict['o'] = None
    dict['r'] = None # reserved
    dict['sr'] = None
    if selected == 'reserved':
        dict['r'] = selected
    if selected == 'manual':
        dict['m'] = selected
    if selected == 'booked':
        dict['b'] = selected
    return dict


def random_price():
    return random.randint(200, 250)


def parse_status(selected):
    if selected == 'open':
        return 'unavailable'
    return 'available'


def init_mock_calendar(
    id: str,
    params: str,
    currency_code: str
):
    if not params:
        raise ValueError('Invalid Start / End Date')

    start_at = to_dt(params['from'])
    end_at = to_dt(params['to'])

    rate: float = get_rate(currency_code)

    day_list = []
    for date in dates_bwn_twodates(start_at, end_at):
        types = ['open', 'booked', 'reserved', 'manual']
        selected = types[random.randint(0, len(types) - 1)]
        day = {
            '__v': '0',
            '_id': '1',
            'accountId': '99',
            'blocks': random_blocks(selected),
            'currency': currency_code,
            'date': from_dt(date),
            'listing': {
                '_id': '1',
                'prices': []
            },
            'listingId': '1',
            'price': int(random_price() * rate),
            'status': parse_status(selected),
        }
        day_list.append(Day(**day).to_dict())
    return day_list


def get_mock_calendar(
    id: str, 
    params: str,
    currency_code: str
):
    logger.info('[MOCK] GET CALENDAR')
    json_data = read_fs_doc('Calendars', str(id))
    if not json_data:
        logger.info('[MOCK] EMPTY CALENDAR')
        calendar_days = init_mock_calendar(
            id=id, 
            params=params,
            currency_code=currency_code
        )
        data: dict = {
            'days': calendar_days
        }
        print(data)
        write_fs_doc('Calendars', id, data)
        return calendar_days
    logger.info('[MOCK] DB CALENDAR')
    return json_data['days']


def get_random_platform():
    platforms = ['airbnb2', 'homeaway2']
    return platforms[random.randint(0, len(platforms)-1)]


def get_random_checkin():
    check_in = datetime.now()
    return check_in + timedelta(days=random.randint(1, 21))


def get_random_fare(nights):
    return random.randint(99, 400) * nights


def get_day_spread(res_days=None):
    """
    Args:
        res_days (array[{start_date: datetime, end_date: datetime}]: An array of datetime objects.
    Returns:
        full_res_days (array[string]): An array of datetime day strings.
    """
    full_res_days = []
    for res in res_days:
        if not 'checkIn' in res or not 'checkOut' in res:
            raise ValueError('Invalid Datetime Object in Day Spread Days')

        rounded_start = res['checkIn'].replace(
            hour=0,
            minute=1,
            second=0,
            microsecond=0
        )
        rounded_end = res['checkOut'].replace(
            hour=0,
            minute=1,
            second=0,
            microsecond=0
        )
        number_days = (rounded_end - rounded_start).days
        for i in range(number_days + 1):
            new_start = rounded_start + timedelta(days=i)
            full_res_days.append(datetime.strftime(new_start, "%Y-%m-%d"))
            i += 1
    return full_res_days


def init_mock_reservations(
    no_list: list, 
    id: str, 
    start_date: str, 
    end_date: str,
    currency_code: str,
):
    rate: float = get_rate(currency_code)
    res_list = []
    num_reservations = random.randint(0, 3)
    for check_in in range(num_reservations):
        # print('GENERATE {} RESERVATIONS'.format(num_reservations))
        nights = random.randint(2, 7)
        check_in = get_random_checkin()
        check_out = check_in + timedelta(days=nights)
        res_spread = get_day_spread([{
            'checkIn': check_in,
            'checkOut': check_out - timedelta(days=1),
        }])
        if check_in in no_list:
            # print('DUPLICATE CHECKIN ATTEMPT')
            num_reservations + 1
            continue


        for res_day in res_spread:
            if res_day in no_list:
                # print('DUPLICATE RES ATTEMPT')
                num_reservations + 1
                continue

        id = str(uuid.uuid4())
        code = str(uuid.uuid4())
        guest = str(uuid.uuid4())
        reservation = {
            '_id': ''.join(x.strip() for x in id.split('-')),
            'accountId': '99',
            'createdAt': from_dtl(datetime.now()),
            'nightsCount': nights,
            'confirmationCode': ''.join(x.strip() for x in code.split('-'))[:6],
            'checkIn': from_dtl(check_in),
            'checkOut': from_dtl(check_out),
            'guestId': ''.join(x.strip() for x in guest.split('-')),
            'integration': {
                '_id': '22',
                'limitations': None,
                'platform': get_random_platform()
            },
            'listingId': id,
            'status': 'confirmed',
            'money': {
                'currency': currency_code,
                'fareAccommodation': int(get_random_fare(nights) * rate),
                'fareCleaning': int(125 * rate),
            }
        }
        res_list.append(reservation)
    return res_list

def get_mock_reservations(
    id: str, 
    start_date: str, 
    end_date: str,
    currency_code: str
):
    logger.info('[MOCK] GET RESERVATIONS')
    json_data = read_fs_docs('Reservations')
    if json_data == []:
        logger.info('[MOCK] EMPTY RESERVATIONS')
        initial_reservations = init_mock_reservations(
            no_list=[],
            id=id,
            start_date=start_date,
            end_date=end_date,
            currency_code=currency_code,
        )
        for res in initial_reservations:
            write_fs_doc('Reservations', str(res['_id']), res)
        
        return initial_reservations

    # res_days = []
    # for reservation in json_data:
    #     res_dict = {
    #         'checkIn': to_dtl(reservation['checkIn']),
    #         'checkOut': to_dtl(reservation['checkOut']) - timedelta(days=1),
    #     }
    #     res_days.append(res_dict)

    # no_list = get_day_spread(res_days)
    # if len(no_list) > 25:
    #     return json_data

    # total_reservations = json_data + init_mock_reservations(
    #     no_list=no_list,
    #     id=id,
    #     start_date=start_date,
    #     end_date=end_date,
    #     currency_code=currency_code
    # )
    # write_fs_doc('Reservations', id, total_reservations)
    logger.info('[MOCK] DB RESERVATIONS')
    return json_data


def listing_images(id: str):
    return [
        'https://www.standout-cabin-designs.com/images/storybook-cottages3-8.jpg',
        'https://www.topsiderhomes.com/images/lhouse.jpg',
        'https://cambusbarron.com/about/images/greirsonhouse.jpg',
        'https://kids.kiddle.co/images/thumb/6/6c/Nathaniel_Jenks_House_Pawtucket_RI.jpg/183px-Nathaniel_Jenks_House_Pawtucket_RI.jpg',
        'https://photos2.fotosearch.com/bthumb/CSP/CSP266/doghouse-clipart__k22315172.jpg'
    ][int(id)]


def get_mock_listing(
    id: str,
    currency_code: str,
):
    rate: float = get_rate(currency_code)
    address: str = '{} Test {}'.format(
        random.randint(1000, 9999),
        random_address_ends[random.randint(0, 2)]
    )
    city: str = 'Newport Beach'
    state: str = 'California'
    postal: str = '92663'
    bathrooms: int = 3
    bedrooms: int = 4
    beds: int = 8
    base_price: int = int(300 * rate)
    cleaning_fee: int = int(100 * rate)
    return {
            "_id": id,
            "accommodates": 12,
            "accountId": "99",
            "active": True,
            "address": {
                "apt": "A",
                "city": city,
                "country": "United States",
                "full": "{} A, {}, {} {}, United States".format(
                    address,
                    city,
                    state,
                    postal
                ),
                "lat": 0,
                "lng": 0,
                "state": state,
                "street": address,
                "zipcode": postal
            },
            "bathrooms": bathrooms,
            "bedrooms": bedrooms,
            "beds": beds,
            "defaultCheckInTime": "17:00",
            "defaultCheckOutTime": "10:00",
            "integrations": [
                {
                    "airbnb2": {
                        "bookingLeadTime": {
                            "allowRequestToBook": False,
                            "hours": 12
                        },
                        "cancellationPolicy": "strict_14_with_grace_period",
                        "daysOfWeekMinimumNights": [],
                        "financials": {
                            "_id": "f99",
                            "securityDepositFee": {
                                "channelSyncStatus": "IN_PROGRESS"
                            }
                        },
                        "id": 22,
                        "instantBookingAllowedCategory": "experienced_guest_with_government_id",
                        "maxDaysNotice": {
                            "allowRequestToBook": True,
                            "days": -1
                        },
                        "status": "COMPLETED",
                        "syncCategory": "sync_all",
                        "syncCategoryUpdatedAt": "2020-12-02T17:24:49.864Z",
                        "turnoverDays": 0
                    },
                    "externalUrl": "https://www.airbnb.com/rooms/22",
                    "homeaway2": None,
                    "id": 22,
                    "platform": "airbnb2"
                },
                {
                    "airbnb2": {
                        "daysOfWeekMinimumNights": []
                    },
                    "externalUrl": None,
                    "homeaway2": {
                        "acceptedPaymentForms": {
                            "methods": [
                                "STRIPE"
                            ],
                            "note": "Accepted forms of payment: Check, Cash, Debit Card, Credit Card,\nMaestro International, Maestro UK, Mastercard, and Visa."
                        },
                        "advertiserId": "33",
                        "bookingPolicy": "QUOTEHOLD",
                        "cancellationPolicy": "MODERATE",
                        "currency": "USD",
                        "financials": {
                            "_id": "f99",
                            "securityDepositFee": {
                                "channelSyncStatus": "IN_PROGRESS"
                            }
                        },
                        "pricingPolicy": "GUARANTEED",
                        "status": "COMPLETED"
                    },
                    "id": "33",
                    "platform": "homeaway2"
                }
            ],
            "isListed": True,
            "nickname": address,
            "picture": {
                "thumbnail": listing_images(id)
            },
            "prices": {
                "basePrice": base_price,
                "cleaningFee": cleaning_fee,
                "currency": currency_code
            }
        }

def get_mock_listings(
    id: str,
    currency_code: str,
):
    logger.info('[MOCK] GET LISTINGS')
    json_data = read_fs_docs('Listings')
    if json_data == []:
        logger.info('[MOCK] EMPTY LISTINGS')
        listings: List[Any] = []
        for i in range(random.randint(1, 5)):
            listing = get_mock_listing(
                str(i + 1),
                currency_code
            )
            write_fs_doc('Listings', str(i + 1), listing)
            listings.append(listing)
        return listings
    logger.info('[MOCK] DB LISTINGS')
    return json_data
    