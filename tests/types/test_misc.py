import pytest
from testing_config import BaseTestConfig
from typing import Dict

from guesty.resource.types import (
    ReservationResponse,
)

class TestMiscTypes(BaseTestConfig):

    # def test_reservation_response(cls):
    #     print('should set reservation response')

    #     dict_pass: dict = cls.json_fixtures['reservations']['post']['leads'][0]
    #     cls.assertEqual(ReservationResponse(**dict_pass).to_dict(), dict_pass)
    
    def test_reservation_response_fail(cls):
        print('should fail to set reservation response')

        dict: dict = cls.json_fixtures['reservations']['get']['results'][0]
        print(ReservationResponse(**dict).to_dict())
        print(dict)
        cls.assertNotEqual(ReservationResponse(**dict).to_dict(), None)