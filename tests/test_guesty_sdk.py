#!/usr/bin/env python
# coding: utf-8

from cmath import e
from testing_config import BaseTestConfig
from unittest.mock import Mock, patch

from datetime import datetime, timedelta

import guesty
from guesty.resource.base import Auth

# import pytest
# @pytest.mark.skip(reason="Using Prod Cert")
class TestGuestySDK(BaseTestConfig):
    """TestGuestySDK."""

    auth: Auth = None
    sdk: guesty.GuestySdk = None

    @patch('guesty.client.requests.post')
    def setUp(cls, mock_post):
        print('should auth')

        mock_post.return_value = Mock(status_code=200)
        mock_post.return_value.json.return_value = cls.json_fixtures['accessToken']['valid']

        cls.auth = guesty.Auth.get_token(
            client_id=cls.json_fixtures['api']['id'],
            client_secret=cls.json_fixtures['api']['secret'],
        )
        cls.assertEqual(cls.auth.to_any_object(), cls.json_fixtures['accessToken']['valid'])
        cls._test_guesty_construct()


    def _test_guesty_construct(cls):
        print('should construct based on access token')
        try:
            # guesty.api_client_id = cls.auth.id
            guesty.api_access_token = cls.auth.access_token
            cls.sdk = guesty.GuestySdk(
                id=cls.json_fixtures['api']['id'],
                expires_in=cls.auth.expires_in,
                token_type=cls.auth.token_type,
                scope=cls.auth.scope,
            )
        except Exception as e:
            print(e)
            cls.fail("GuestySdk() raised Exception unexpectedly!")


    # TEST: get_listing
    @patch('guesty.client.requests.get')
    def test_guesty_get_listing(cls, mock_get):
        print('should get listing')
        
        mock_get.return_value = Mock(status_code=200)
        mock_get.return_value.json.return_value = cls.json_fixtures['listings']

        response = cls.sdk.get_listings()
        cls.assertEqual(len(response), 1)  # TODO: Map entire object json_fixture != model
        print('TESTED: get_listing')

    # TEST: get_calendar
    @patch('guesty.client.requests.get')
    def test_guesty_get_calendar(cls, mock_get):
        print('should get calendar')

        start_date = datetime.now()
        end_date = start_date + timedelta(days=365)

        mock_get.return_value = Mock(status_code=200)
        mock_get.return_value.json.return_value = cls.json_fixtures['calendar']['get']

        response = cls.sdk.get_calendar(
            '5efcefeb0e892d002892bc16',  # Listing ID
            datetime.strftime(start_date, '%Y-%m-%d'),
            datetime.strftime(end_date, '%Y-%m-%d'),
        )
        cls.assertEqual(
            len(response.to_dict()['days']),
            len(cls.json_fixtures['calendar']['get'])
        )
        print('TESTED: get_calendar')

    # TEST: get_reservations
    @patch('guesty.client.requests.get')
    def test_guesty_get_reservations(cls, mock_get):
        print('should get reservations')

        start_date = datetime.now() - timedelta(days=30)
        end_date = start_date + timedelta(days=365)

        mock_get.return_value = Mock(status_code=200)
        mock_get.return_value.json.return_value = cls.json_fixtures['reservations']['get']

        response = cls.sdk.get_reservations(
            '5ed7b1f968fd190029136dbe',  # Listing ID
            datetime.strftime(start_date, '%Y-%m-%d'),
            datetime.strftime(end_date, '%Y-%m-%d'),
        )
        cls.assertEqual(len(response), 1)  # TODO: Map entire object json_fixture != model
        print('TESTED: get_reservations')

    # TEST: get_reservations (empty)
    @patch('guesty.client.requests.get')
    def test_guesty_get_reservations_empty(cls, mock_get):
        print('should get reservations empty')

        start_date = datetime.now()
        end_date = start_date + timedelta(days=365)

        mock_get.return_value = Mock(status_code=200)
        mock_get.return_value.json.return_value = cls.json_fixtures['reservations']['empty']

        response = cls.sdk.get_reservations(
            '5efcefeb0e892d002892bc16',  # Listing ID
            datetime.strftime(start_date, '%Y-%m-%d'),
            datetime.strftime(end_date, '%Y-%m-%d'),
        )
        cls.assertEqual(len(response), 0)
        print('TESTED: get_reservations (empty)')

    # TEST: update_calendar
    @patch('guesty.client.requests.put')
    def test_guesty_update_calendar(cls, mock_put):
        print('should update calendars by array')
        
        batch_array = [{
            'from': '2022-07-29',
            'to': '2022-07-29',
            'price': 100,
          },
          {
            'from': '2022-07-30',
            'to': '2022-07-30',
            'price': 100,
        }]

        mock_put.return_value = Mock(status_code=200)
        mock_put.return_value.json.return_value = cls.json_fixtures['calendar']['put']

        response = cls.sdk.update_calendar(
            '5efcefeb0e892d002892bc16',
            batch_array,
        )
        cls.assertNotEqual(response, 200)
        print('TESTED: update_calendar')